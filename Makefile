SOURCE_PATH=$(PWD)/ska-pss-sandpit-2
BUILD_PATH=$(PWD)/ska-pss-sandpit-2/build
SOURCE_PATH_2=$(PWD)/ska-pss-sandpit-3
BUILD_PATH_2=$(PWD)/ska-pss-sandpit-3/build
.PHONY: build build_2

build:
	@echo "START: build"
	mkdir $(BUILD_PATH)
	cmake -S $(SOURCE_PATH) -B $(BUILD_PATH)
	cmake --build $(BUILD_PATH)
	$(BUILD_PATH)/app
	@echo "END: build"
	@echo "START: build 2"
	mkdir $(BUILD_PATH_2)
	cmake -S $(SOURCE_PATH_2) -B $(BUILD_PATH_2)
	cmake --build $(BUILD_PATH_2)
	$(BUILD_PATH_2)/app_2

build_2:	
	@echo "START: build_2 !!"
	cd $(SOURCE_PATH_2)
	ls -lR
	mkdir build
	cd build
	cmake $(SOURCE_PATH_2)
	cmake --build .
	./app_2 . 
	@echo "END: build_2 !!"

# test:	
# 	@echo "START: test!!"
# 	cd $(SOURCE_PATH_2)
# 	mkdir build
# 	cd build
# 	cmake $(SOURCE_PATH_2)
# 	cmake --build .
# 	./app_2 .
# 	@echo "END: test"
